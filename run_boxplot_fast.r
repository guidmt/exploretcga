#questo plot serve a fare vedere i livelli di espressione di n geni in una matrice.

#example Rscript run_boxplot_fast.r matrix_tcga_rnaseq_MYC_ANP32E_amplified.txt gene_list_AR_ESR1_PGR expression_rnaseq_AR_ESR1_PGR.pd
#tab is a matrix
args <- commandArgs(trailingOnly = TRUE)
tab<-args[1]
#gene_list is a file where in the first column is a list of genes
gene_list <- args[2]

#output file pdf
output<-args[3]


add.alpha <- function(col, alpha=1){
  if(missing(col))
    stop("Please provide a vector of colours.")
  apply(sapply(col, col2rgb)/255, 2,
                     function(x)
                       rgb(x[1], x[2], x[3], alpha=alpha))
}

tab_input<-read.table(file=tab,sep="\t",stringsAsFactors=F,header=T,skip=2)
gene_list_input<-read.table(file=gene_list,sep="\t",stringsAsFactors=F)

print("i skip the first two lines because are characters")

tab_subset<-tab_input[tab_input[,1]%in%gene_list_input[,1],]

pdf(output)

        blue="steelblue3"
        blue_alpha<-add.alpha(blue,0.4)
	boxplot(t(tab_subset[-1,-1]),type="l",lty=1,xaxt="n",lwd=1.5,col=blue_alpha,outcol=blue_alpha,pch=19,ylab="RSEM")
	axis(1,las=2,labels=tab_subset[,1],at=1:length(tab_subset[,1]),cex.axis=1)
		
dev.off()

