library(survival)
##

annotation_file<-"FALSE"
output_dir<-"chromatin_players_km_vital_status_all"

setwd("/media/data_disk/TCGA/new_TCGA/results/ANP32E_MYC_amplification")

annotation<-read.table(file="breast_cancer_samples_with_amplification_ANP32E_MYC.txt",sep="\t",stringsAsFactors=F)
setwd("/media/data_disk/TCGA/new_TCGA")
list_of_genes<-read.table(file="chromatin_regulators_list_AZ.txt",sep="\t",stringsAsFactors=F,header=F)

setwd("/media/data_disk/TCGA/new_TCGA")
list_of_genes<-list_of_genes[,1]


setwd("/media/data_disk/TCGA/new_TCGA/TCGA_BRCA_exp_HiSeqV2-2015-02-24") 

tab<-read.table(file="clinical_data",sep="\t",header=T)

strings<-c("sampleID","days_to_new_tumor_event_after_initial_treatment","Metastasis_nature2012","Vital_Status_nature2012","days_to_last_followup","followup_case_report_form_submission_reason","days_to_last_known_alive","lost_follow_up","days_to_death","vital_status","days_to_collection")

strings<-paste(strings,collapse="|")

column<-grep(colnames(tab),pattern=strings,value=T)

tabselect<-tab[,column]
tabselect[,1]<-gsub(tabselect[,1],pattern="-",replacement=".")

####Nel primo caso voglio fare la curva di soppravvivenza dei pazienti che hanno o non hanno l'amplificazione di ANP32E e MYC
####Per questa analisi recupero il codice che ho usato per integrare i dati.
###questa analalisi è stata fatta usando tutto il dataset non ho filtrato per cnv
setwd("/media/data_disk/TCGA/new_TCGA/TCGA_BRCA_exp_HiSeqV2-2015-02-24/")

tabrnaseq<-read.table(file="genomicMatrix",sep="\t",stringsAsFactors=F,header=T)

if(annotation_file=="TRUE"){

tabselect_tabrnaseq<-tabrnaseq[,which(colnames(tabrnaseq) %in% annotation[,1])]

tabrnaseq<-data.frame(tabrnaseq[,1], tabselect_tabrnaseq)

list_of_genes<-intersect(tabrnaseq[,1],list_of_genes)

}

#the genes in your genes list could be present in the matrix of gene-expression
list_of_genes<-intersect(tabrnaseq[,1],list_of_genes)

#time<-"days_to_last_followup"
time<-"days_to_death"
#time<-"days_to_collection"
#time<-"days_to_last_known_alive"
setwd("/media/data_disk/TCGA/new_TCGA/results/ANP32E_MYC_amplification")

command<-paste("mkdir",output_dir,sep=" ")

system(command)

dirchange<-paste("/media/data_disk/TCGA/new_TCGA/results/ANP32E_MYC_amplification",output_dir,sep="/")

setwd(dirchange)

for (i in list_of_genes){

###
###  In this step i want create a new df for the survival analysis
###

selected_gene<-i

#method<-"mean"
##method can assume mean value, quantile and selected_value
##qual_parameter is threshold to use to select the data
##value_for_selecte_value is the gene expression value that you want consider

method<-"quantile"
qual_parameter<-"50%"
value_for_selected_value<-10
event<-"vital_status"

if(method=="mean"){

if(rowSums(tabrnaseq[tabrnaseq[,1]==selected_gene,-1])!=0) {

subtab<-tabrnaseq[tabrnaseq[,1]==selected_gene,]

mean.geneforsamples<-mean(as.numeric((subtab[,-1])))

subtabfilter.positive<-subtab[, which(subtab[,-1]>mean.geneforsamples)]
subtabfilter.negative<-subtab[, which(subtab[,-1]<mean.geneforsamples)]

if(nrow(subtabfilter.positive)!=0){

object_positive<-data.frame(sampleID=colnames(subtabfilter.positive),status=rep(1,nrow(subtabfilter.positive)))
object_negative<-data.frame(sampleID=colnames(subtabfilter.negative),status=rep(0,length(subtabfilter.negative)))

tab_for_survival<-rbind(object_positive,object_negative)
}

}

}

if(method=="quantile"){

if(rowSums(tabrnaseq[tabrnaseq[,1]==selected_gene,-1])!=0) {

subtab<-tabrnaseq[tabrnaseq[,1]==selected_gene,]

quantile.geneforsamples<-quantile(as.numeric((subtab[,-1])))
quantilevl.geneforsamples<-quantile.geneforsamples[names(quantile.geneforsamples)==qual_parameter]

subtabfilter.positive<-subtab[, which(subtab[,-1]>quantilevl.geneforsamples)]
subtabfilter.negative<-subtab[, which(subtab[,-1]<quantilevl.geneforsamples)]

if(nrow(subtabfilter.positive)!=0){

object_positive<-data.frame(sampleID=colnames(subtabfilter.positive),status=rep(1,nrow(subtabfilter.positive)))
object_negative<-data.frame(sampleID=colnames(subtabfilter.negative),status=rep(0,length(subtabfilter.negative)))

tab_for_survival<-rbind(object_positive,object_negative)
}

}

}

if(method=="selected_value"){

if(rowSums(tabrnaseq[tabrnaseq[,1]==selected_gene,-1])!=0) {

subtab<-tabrnaseq[tabrnaseq[,1]==selected_gene,]

subtabfilter.positive<-subtab[, which(subtab[,-1]>value_for_selected_value)]
subtabfilter.negative<-subtab[, which(subtab[,-1]<value_for_selected_value)]

if(nrow(subtabfilter.positive)!=0){

object_positive<-data.frame(sampleID=colnames(subtabfilter.positive),status=rep(1,nrow(subtabfilter.positive)))
object_negative<-data.frame(sampleID=colnames(subtabfilter.negative),status=rep(0,length(subtabfilter.negative)))

tab_for_survival<-rbind(object_positive,object_negative)
}

}

}



## check if are present some patients without the death and with only the followup.
## We want remove this information, because after the last day of the follow-up we
## don't have information about the patient. 

tab_for_survival_input<-merge(tab_for_survival,tabselect,by="sampleID")
#event<-"Metastasis_nature2012"
#event<-"vital_status"
#event<-"Vital_Status_nature2012"

#first filter
tab_for_survival_input<-tab_for_survival_input[-(which(tab_for_survival_input[,event]=="")),]

label<-unique(tab_for_survival_input[,event])[1]

tab_for_survival_input$death_event <- ifelse(tab_for_survival_input[,event] == label, 0,1)

#first object: time
#second object: death event (morte o vivo) 
#third object: is your genes up or down-regulate (status)

##time 1 1 1 #time1, death yes(1), copy-number(1)
##time 2 0 0 #time2, death no (0)), copy-number(0)
##check that the class in the survival curve are two
if(length(unique(tab_for_survival_input$status))==2) {

res.surv.positive <- survfit(Surv(tab_for_survival_input[,time],tab_for_survival_input$death_event)~ tab_for_survival_input$status)

input_pval<-tryCatch(survdiff(Surv(tab_for_survival_input[,time],tab_for_survival_input$death_event)~ tab_for_survival_input$status))
pv <- ifelse(is.na(input_pval),next,(round(1 - pchisq(input_pval$chisq, length(input_pval$n) - 1),3)))[[1]]
}
#res.surv.positive <- survfit(Surv(tab_for_survival_input$days_to_last_followup, tab_for_survival_input$days_to_death)~ strata(tab_for_survival_input$status), conf.type="none")

##
## In this plot remember that in the legend the first element correspond to the status where is not true a condition, while the secondo element to which where is true.
## for doubt check the scale of time and the respectivelly  values of status (with copy number or not)  str(tab_for_survival_input[,c(3,2)])
##

if(annotation_file=="TRUE"){

output<-paste(paste("km_overexpressed_selectedsamples",selected_gene,event,time,pv,sep="_"),".pdf",sep="")

} else{

output<-paste(paste("km_overexpressed_allsamples",selected_gene,event,time,pv,sep="_"),".pdf",sep="")

}

pdf(output)
#il colore è riferito a quello che c'è dentro strata
plot(res.surv.positive,xlab="Time (days)", ylab="Survival Probability",col=c("blue","red"),pch="l",main=selected_gene)
plot(1:10,pch="")
legend(6,8,legend=paste("p.value = ",pv[[1]],sep=""),bty="n",cex=1)
legend("center",legend=c("Not_Overexpression","Overexpression"), col=c("blue","red"),cex=1,bty="n", lty = 1)
dev.off()


}


#####
##### Now I used the signatures obatined previously 
#####

