#Rscript /media/data_disk/script_utility/TCGA_script/extractMatrixFromGenelist.r /media/data_disk/TCGA/new_TCGA/TCGA_BRCA_exp_HiSeqV2-2015-02-24/genomicMatrix gene_list_DEGs_basal_normal_0.05_1.5.txt matrixDEGs_normal_basal_0.05_1.5.txt
##

##usage
args <- commandArgs(trailingOnly = TRUE)

tab<-args[[1]]
genelist<-args[[2]]
output<-args[[3]]

print(tab)

tab_input<-read.table(file=tab,sep="\t",stringsAsFactors=F,header=T)
print("Read the matrix")
genelist_input<-read.table(file=genelist,sep="\t",stringsAsFactors=F,header=F)
print("Read the genelist")
tab_select<-tab_input[tab_input[,1]%in%genelist_input[,1],]

write.table(tab_select,file=output,sep="\t",row.names=F,quote=F,col.names=T)
