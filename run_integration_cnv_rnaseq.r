##Questo script serve a vedere i livelli di espressione di un gene in relazione al cnv
setwd("/media/data_disk/TCGA/new_TCGA/TCGA_BRCA_gistic2thd-2015-02-24")

add.alpha <- function(col, alpha=1){
  if(missing(col))
    stop("Please provide a vector of colours.")
  apply(sapply(col, col2rgb)/255, 2,
                     function(x)
                       rgb(x[1], x[2], x[3], alpha=alpha))
}


gene_for_analysis<-"MYC"

tab_cnv<-read.table(file="genomicMatrix",sep="\t",stringsAsFactors=F,header=T)

setwd("/media/data_disk/TCGA/new_TCGA/TCGA_BRCA_exp_HiSeqV2-2015-02-24")

tab_rnaseq<-read.table(file="genomicMatrix",sep="\t",stringsAsFactors=F,header=T)

tab_cnv_select_gene<-tab_cnv[tab_cnv[,1]==gene_for_analysis,]
tab_rnaseq_select_gene<-tab_rnaseq[tab_rnaseq[,1]==gene_for_analysis,]

status_gistic<-c(-2,-1,0,1,2)

setwd("/media/data_disk/TCGA/new_TCGA/results/ANP32E_MYC_amplification")

output_file<-paste("relation_rnaseq_cnv_allsamples",gene_for_analysis,sep="_")

pdf(output_file)

par(mar=c(8,5,2,2))
list_data<-list(1,2,3,4,5)

names(list_data)<-c("Deep loss","Shallow loss","Diploid","Gain","Amplification")

vector_gistic<-length(status_gistic)

for(i in 1:vector_gistic){

status_gistic_select<-status_gistic[i]

select_column_status<-tab_cnv_select_gene[tab_cnv_select_gene %in% status_gistic_select]

if(ncol(select_column_status)==0){

next

} else {

tab_for_boxplot<-tab_rnaseq_select_gene[,colnames(tab_rnaseq_select_gene) %in% colnames(select_column_status),]

list_data[[i]]<- t(tab_for_boxplot)


}


}
	blue4="steelblue4"
	blue_alpha4<-add.alpha(blue4,0.4)

	
        blue3="steelblue3"
        blue_alpha3<-add.alpha(blue3,0.4)
	
	snow4="snow4"
	snow4_alpha<-add.alpha(snow4,0.4)
	
	red3="red3"
	red3_alpha<-add.alpha(red3,0.4)
	
	red4="red4"
	red4_alpha<-add.alpha(red4,0.4)
	
        boxplot(list_data,type="l",lty=1,xaxt="n",lwd=1.5,col=c(blue_alpha4,blue_alpha3,snow4_alpha,red3_alpha,red4_alpha),outcol=c(blue_alpha4,blue_alpha3,snow4_alpha,red3_alpha,red4_alpha),pch=19,ylab="RSEM",main=gene_for_analysis)
	
	axis(1,las=2,labels=names(list_data),at=1:vector_gistic,cex.axis=1)

dev.off()
