#ex run_km.r function
#Rscript run_km_cnv_2genes.r /media/data_disk/TCGA/new_TCGA/TCGA_BRCA_exp_HiSeqV2-2015-02-24/clinical_data /media/data_disk/TCGA/new_TCGA/TCGA_BRCA_gistic2thd-2015-02-24/genomicMatrix ANP32E MYC breast_cancer_samples_with_amplification_ANP32E_MYC.txt
args <- commandArgs(trailingOnly = TRUE)

clinical_data<-args[1]
tab_cnv<-args[2]
gene_for_analysis_1<-args[3]
gene_for_analysis_2<-args[4]
output_samples<-args[5]

library(survival)

tab<-read.table(file=clinical_data,sep="\t",header=T)

strings<-c("sampleID","days_to_new_tumor_event_after_initial_treatment","days_to_last_followup","Metastasis_nature2012","followup_case_report_form_submission_reason","lost_follow_up","days_to_death","vital_status")

strings<-paste(strings,collapse="|")

column<-grep(colnames(tab),pattern=strings,value=T)

tabselect<-tab[,column]
tabselect[,1]<-gsub(tabselect[,1],pattern="-",replacement=".")

####Nel primo caso voglio fare la curva di soppravvivenza dei pazienti che hanno o non hanno l'amplificazione di ANP32E e MYC
####Per questa analisi recupero il codice che ho usato per integrare i dati.
tab_cnv<-read.table(file=tab_cnv,sep="\t",stringsAsFactors=F,header=T)

tab_anp32e<-tab_cnv[which(tab_cnv[,1]==gene_for_analysis_1),]
sample_tab_anp32e<-tab_anp32e[which(tab_anp32e[1,]>=1)]

tab_myc<-tab_cnv[which(tab_cnv[,1]==gene_for_analysis_2),]
sample_tab_myc<-tab_myc[which(tab_myc[1,]>=1)]

selected_genes<-c(which(tab_cnv[,1]==gene_for_analysis_1),which(tab_cnv[,1]==gene_for_analysis_2))

tab_both<-tab_cnv[selected_genes,]
sample_tab_both<-data.frame(which(colSums(tab_both[,c(2:ncol(tab_both))])>=2))

write.table(rownames(sample_tab_both),file=output_samples,sep="\t",col.names=F,row.names=F,quote=F)

###
###  In this step i want create a new df for the survival analysis
###

#time<-"days_to_last_followup"
time<-"days_to_death"
#time<-"days_to_collection"
#time<-"days_to_last_known_alive"

event<-"vital_status"
#event<-"Vital_Status_nature2012"
#event<-"Metastasis_nature2012"

object_positive<-data.frame(sampleID=rownames(sample_tab_both),status=rep(1,nrow(sample_tab_both)))
negative_class<-setdiff(tabselect[,1],object_positive[,1])

object_negative<-data.frame(sampleID=negative_class,status=rep(0,length(negative_class)))
tab_for_survival<-rbind(object_positive,object_negative)

## check if are present some patients without the death and with only the followup.
## We want remove this information, because after the last day of the follow-up we
## don't have information about the patient. 

tab_for_survival_input<-merge(tab_for_survival,tabselect,by="sampleID")

#first filter
tab_for_survival_input<-tab_for_survival_input[-(which(tab_for_survival_input[,event]=="")),]

#here i used a second filter, for some samples is absent the date of death
#tab_for_survival_input<-tab_for_survival_input[which(!is.na(tab_for_survival_input[,3])),]
#put zero for 
label<-unique(tab_for_survival_input[,event])[1]

tab_for_survival_input$death_event <- ifelse(tab_for_survival_input[,event] == label, 0,1)

#first object: time
#second object: death event
#third object: is your genes up or down-regulatee

##time 1 1 1 #time1, death yes(1), copy-number(1)
##time 2 0 0 #time2, death no (0)), copy-number(0)
res.surv.positive <- survfit(Surv(tab_for_survival_input[,time],tab_for_survival_input$death_event)~ tab_for_survival_input$status)

input_pval<-tryCatch(survdiff(Surv(tab_for_survival_input[,time],tab_for_survival_input$death_event)~ tab_for_survival_input$status))
pv <- ifelse(is.na(input_pval),next,(round(1 - pchisq(input_pval$chisq, length(input_pval$n) - 1),3)))[[1]]

#res.surv.positive <- survfit(Surv(tab_for_survival_input$days_to_last_followup, tab_for_survival_input$days_to_death)~ strata(tab_for_survival_input$status), conf.type="none")

##
## In this plot remember that in the legend the first element correspond to the status where is not true a condition, while the secondo element to which where is true.
## for doubt check the scale of time and the respectivelly  values of status (with copy number or not)  str(tab_for_survival_input[,c(3,2)])
##

output<-paste(paste("km_amplification",gene_for_analysis_1,gene_for_analysis_2,time,event,sep="_"),".pdf",sep="")

pdf(output)
#il colore è riferito a quello che c'è dentro strata
plot(res.surv.positive,xlab="Time (days)", ylab="Survival Probability",col=c("blue","red"),pch="l",main="KM ANP32E, MYC AMPLIFICATION")
plot(1:10,pch="")
legend(6,8,legend=paste("p.value = ",pv[[1]],sep=""),bty="n",cex=1)
legend("center",legend=c("Not_Amplification","Amplification"), col=c("blue","red"),cex=1,bty="n", lty = 1)

dev.off()

##################
################## Repeat the same analysis using only GENEA
##################
object_positive<-data.frame(sampleID=colnames(sample_tab_anp32e),status=rep(1,nrow(sample_tab_anp32e)))
negative_class<-setdiff(tabselect[,1],object_positive[,1])

object_negative<-data.frame(sampleID=negative_class,status=rep(0,length(negative_class)))
tab_for_survival<-rbind(object_positive,object_negative)

tab_for_survival_input<-merge(tab_for_survival,tabselect,by="sampleID")

#first filter
tab_for_survival_input<-tab_for_survival_input[-(which(tab_for_survival_input$vital_status=="")),]

#here i used a second filter, for some samples is absent the date of death
#tab_for_survival_input<-tab_for_survival_input[which(!is.na(tab_for_survival_input[,3])),]

tab_for_survival_input$death_event <- ifelse(tab_for_survival_input$vital_status == "LIVING", 0,1)

#first object: time
#second object: death event
#third object: is your genes up or down-regulatee

##time 1 1 1 #time1, death yes(1), copy-number(1)
##time 2 0 0 #time2, death no (0)), copy-number(0)
res.surv.positive <- survfit(Surv(tab_for_survival_input$days_to_death,tab_for_survival_input$death_event)~ tab_for_survival_input$status)

input_pval<-tryCatch(survdiff(Surv(tab_for_survival_input$days_to_death,tab_for_survival_input$death_event)~ tab_for_survival_input$status))
pv <- ifelse(is.na(input_pval),next,(round(1 - pchisq(input_pval$chisq, length(input_pval$n) - 1),3)))[[1]]

output<-paste(paste("km_amplification",gene_for_analysis_1,time,event,sep="_"),".pdf",sep="")

pdf(output)
#il colore è riferito a quello che c'è dentro strata
plot(res.surv.positive,xlab="Time (days)", ylab="Survival Probability",col=c("blue","red"),pch="l",main="KM ANP32E AMPLIFICATION")

#legend(4000,0.95,legend=c("Not_Amplification","Amplification"), col=c("blue","red"),cex=1,bty="n", lty = 1)
plot(1:10,pch="")
legend(6,8,legend=paste("p.value = ",pv[[1]],sep=""),bty="n",cex=1)
legend("center",legend=c("Not_Amplification","Amplification"), col=c("blue","red"),cex=1,bty="n", lty = 1)

dev.off()

################## gene b
object_positive<-data.frame(sampleID=colnames(sample_tab_myc),status=rep(1,nrow(sample_tab_myc)))
negative_class<-setdiff(tabselect[,1],object_positive[,1])

object_negative<-data.frame(sampleID=negative_class,status=rep(0,length(negative_class)))
tab_for_survival<-rbind(object_positive,object_negative)

tab_for_survival_input<-merge(tab_for_survival,tabselect,by="sampleID")

#first filter
tab_for_survival_input<-tab_for_survival_input[-(which(tab_for_survival_input$vital_status=="")),]

#here i used a second filter, for some samples is absent the date of death
#tab_for_survival_input<-tab_for_survival_input[which(!is.na(tab_for_survival_input[,3])),]

tab_for_survival_input$death_event <- ifelse(tab_for_survival_input$vital_status == "LIVING", 0,1)

#first object: time
#second object: death event
#third object: is your genes up or down-regulatee

##time 1 1 1 #time1, death yes(1), copy-number(1)
##time 2 0 0 #time2, death no (0)), copy-number(0)
res.surv.positive <- survfit(Surv(tab_for_survival_input$days_to_death,tab_for_survival_input$death_event)~ tab_for_survival_input$status)

input_pval<-tryCatch(survdiff(Surv(tab_for_survival_input$days_to_death,tab_for_survival_input$death_event)~ tab_for_survival_input$status))
pv <- ifelse(is.na(input_pval),next,(round(1 - pchisq(input_pval$chisq, length(input_pval$n) - 1),3)))[[1]]


output<-paste(paste("km_amplification",gene_for_analysis_2,time,event,sep="_"),".pdf",sep="")

pdf(output)

plot(res.surv.positive,xlab="Time (days)", ylab="Survival Probability",col=c("blue","red"),pch="l",main="KM MYC AMPLIFICATION")
plot(1:10,pch="")
legend(6,8,legend=paste("p.value = ",pv[[1]],sep=""),bty="n",cex=1)
legend("center",legend=c("Not_Amplification","Amplification"), col=c("blue","red"),cex=1,bty="n", lty = 1)

dev.off()

