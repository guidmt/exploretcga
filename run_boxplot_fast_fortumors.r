##questo plot serve a fare vedere quali sono i livelli di espressione di una serie di geni in relazione al tipo di tumore 

##Rscript  run_boxplot_fast_fortumors.r matrix_tcga_rnaseq_MYC_ANP32E_amplified.txt gene_list_AR_ESR1_PGR association_samples_pam50_MYC_ANP32E_amplified.txt expression_rnaseq_11genes.pdf

#tab is a matrix
args <- commandArgs(trailingOnly = TRUE)
tab<-args[1]
#gene_list is a file where in the first column is a list of genes
gene_list <- args[2]
annotation<-args[3]
#output file pdf
output<-args[4]


add.alpha <- function(col, alpha=1){
  if(missing(col))
    stop("Please provide a vector of colours.")
  apply(sapply(col, col2rgb)/255, 2,
                     function(x)
                       rgb(x[1], x[2], x[3], alpha=alpha))
}

tab_input<-read.table(file=tab,sep="\t",stringsAsFactors=F,header=T,skip=1)
gene_list_input<-read.table(file=gene_list,sep="\t",stringsAsFactors=F)
tab_annotation<-read.table(file=annotation,sep="\t",stringsAsFactors=F,header=T)
list_tumors<-unique(tab_annotation[,2])

rownames(tab_input)<-tab_input[,1]

pdf(output)

for (i in list_tumors){

tab_annotation_tumors<-tab_annotation[tab_annotation[,2]==i,]

tab_input_tumors<-tab_input[,colnames(tab_input)[-1] %in% tab_annotation_tumors[,1]]

print("i skip the first two lines because are characters")

tab_subset<-tab_input_tumors[rownames(tab_input_tumors)%in%gene_list_input[,1],]


        blue="steelblue3"
        blue_alpha<-add.alpha(blue,0.4)
	boxplot(t(tab_subset[,-1]),type="l",lty=1,xaxt="n",lwd=1.5,col=blue_alpha,outcol=blue_alpha,pch=19,ylab="RSEM",main=i)
	axis(1,las=2,labels=rownames(tab_subset),at=1:length(rownames(tab_subset)),cex.axis=1)
}
		
dev.off()

