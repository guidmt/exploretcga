#Rscript  /media/data_disk/TCGA/new_TCGA/TCGA_BRCA_exp_HiSeqV2-2015-02-24/genomicMatrix /media/data_disk/TCGA/new_TCGA/results/ANP32E_MYC_amplification/association_samples_pam50_MYC_ANP32E_amplified.txt Basal matrix_rnaseq_basal.txt
args <- commandArgs(trailingOnly = TRUE)

tab<-args[[1]]
sampleinfo<-args[[2]]
cancer_subtype<-args[[3]]
output<-args[[4]]

print(tab)

tab_input<-read.table(file=tab,sep="\t",stringsAsFactors=F,header=T)
print("Read the matrix")
sampleinfo<-read.table(file=sampleinfo,sep="\t",stringsAsFactors=F,header=F)
sampleinfo_cancer_subtype<-sampleinfo[sampleinfo[,2]%in%cancer_subtype,]


print("Read the genelist")
tab_select<-tab_input[,colnames(tab_input) %in% sampleinfo_cancer_subtype[,1]]

tab_select<-data.frame(tab_input[,1],tab_select)

write.table(tab_select,file=output,sep="\t",row.names=F,quote=F,col.names=T)

