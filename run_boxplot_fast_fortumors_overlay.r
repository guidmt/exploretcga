##questo plot serve a fare vedere quali sono i livelli di espressione di una serie di geni in relazione al tipo di tumore 

###Rscript /media/data_disk/script_utility/TCGA_script/run_boxplot_fast_fortumors_overlay.r matrixDEGs_normal_basal_0.05_1.5.txt gene_list_AR_ESR1_PGR association_samples_pam50_MYC_ANP32E_amplified.txt "Normal,Basal" ANP323_basal_normal_allsamples.pdf
###
### This script is similar to run_boxplot_fast_fortumors.r but perfom the overlap of the boxplot.
###

#tab is a matrix
args <- commandArgs(trailingOnly = TRUE)
tab<-args[1]
#gene_list is a file where in the first column is a list of genes
gene_list <- args[2]
annotation<-args[3]
list_condition<-args[4]

list_condition_a<-strsplit(args[4],split=",")[[1]][1]
list_condition_b<-strsplit(args[4],split=",")[[1]][2]

print(list_condition_a)
print(list_condition_b)

list_condition<-c(list_condition_a,list_condition_b)

#output file pdf
output<-args[5]

add.alpha <- function(col, alpha=1){
  if(missing(col))
    stop("Please provide a vector of colours.")
  apply(sapply(col, col2rgb)/255, 2,
                     function(x)
                       rgb(x[1], x[2], x[3], alpha=alpha))
}

tab_input<-read.table(file=tab,sep="\t",stringsAsFactors=F,header=T)
gene_list_input<-read.table(file=gene_list,sep="\t",stringsAsFactors=F)
tab_annotation<-read.table(file=annotation,sep="\t",stringsAsFactors=F,header=T)

tab_annotation_1<-tab_annotation[tab_annotation[,2]%in%list_condition[1],]
tab_annotation_2<-tab_annotation[tab_annotation[,2]%in%list_condition[2],]

tab_annotation<-rbind(tab_annotation_1,tab_annotation_2)

list_tumors<-unique(tab_annotation[,2])

rownames(tab_input)<-tab_input[,1]

list_colors<-c("steelblue3","red4")

pdf(output)

newdf<-list(1:2)

for (i in 1:length(list_tumors)){

select_tumor<-list_tumors[i]

print(select_tumor)

#the name of the rows is the gene-id
rownames(tab_input)<-tab_input[,"sample"]

tab_annotation_tumors<-tab_annotation[tab_annotation[,2]==select_tumor,]

tab_input_tumors<-tab_input[,colnames(tab_input)[-1] %in% tab_annotation_tumors[,1]]

tab_subset<-tab_input_tumors[rownames(tab_input_tumors)%in%gene_list_input[,1],]

newdf[[i]]<-tab_subset

}	

names(newdf)<-list_condition

        alpha_blue<-add.alpha(list_colors[1],0.4)
	alpha_red<-add.alpha(list_colors[2],0.4)
	
	boxplot(t(newdf[[1]]),type="l",lty=1,xaxt="n",lwd=1.0,col=alpha_blue,outcol=alpha_blue,pch=19,ylab="RSEM",ylim=c(0,20))
	boxplot(t(newdf[[2]]),type="l",lty=1,xaxt="n",lwd=1.0,col=alpha_red,outcol=alpha_red,pch=19,add=T)

	axis(1,las=2,labels=rownames(tab_subset),at=1:length(rownames(tab_subset)),cex.axis=1)

	plot(1:10,col="white")
	legend("topright",c(list_condition[1],list_condition[2]),fill=c(alpha_blue,alpha_red),cex=5)
	
dev.off()

