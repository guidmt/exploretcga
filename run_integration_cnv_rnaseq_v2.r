#Rscript run_integration_cnv_rnaseq_v2.r /media/data_disk/TCGA/new_TCGA/TCGA_BRCA_gistic2thd-2015-02-24/genomicMatrix  /media/data_disk/TCGA/new_TCGA/TCGA_BRCA_exp_HiSeqV2-2015-02-24/genomicMatrix MYC relation_rnaseq_cnv_allsamples_v2 TRUE
#Rscript run_integration_cnv_rnaseq_v2.r /media/data_disk/TCGA/new_TCGA/TCGA_BRCA_gistic2thd-2015-02-24/genomicMatrix /media/data_disk/TCGA/new_TCGA/results/ANP32E_MYC_amplification/matrix_tcga_rnaseq_MYC_ANP32E_amplified.ok.txt ANP32E relation_rnaseq_cnv_MYC_ANP32_amplified_v2 TRUE
args <- commandArgs(trailingOnly = TRUE)

tab_cnv<-args[1]
tab_rnaseq<-args[2]
gene_for_analysis<-args[3]
output_file<-args[4]
fit<-args[5]

add.alpha <- function(col, alpha=1){
  if(missing(col))
    stop("Please provide a vector of colours.")
  apply(sapply(col, col2rgb)/255, 2,
                     function(x)
                       rgb(x[1], x[2], x[3], alpha=alpha))
}


tab_cnv<-read.table(file=tab_cnv,sep="\t",stringsAsFactors=F,header=T)

tab_rnaseq<-read.table(file=tab_rnaseq,sep="\t",stringsAsFactors=F,header=T)

tab_cnv_select_gene<-tab_cnv[tab_cnv[,1]==gene_for_analysis,]
tab_rnaseq_select_gene<-tab_rnaseq[tab_rnaseq[,1]==gene_for_analysis,]

status_gistic<-c(-2,-1,0,1,2)

output_file<-paste(paste(output_file,gene_for_analysis,sep="_"),".pdf",sep="")

pdf(output_file)

par(mar=c(8,5,2,2))
list_data<-list(1,2,3,4,5)

names(list_data)<-c("Deep loss","Shallow loss","Diploid","Gain","Amplification")

vector_gistic<-length(status_gistic)

for(i in 1:vector_gistic){

status_gistic_select<-status_gistic[i]

select_column_status<-tab_cnv_select_gene[tab_cnv_select_gene %in% status_gistic_select]

if(ncol(select_column_status)==0){

next

} else {

if(length(intersect(colnames(tab_rnaseq_select_gene),colnames(select_column_status)))==0)

	{next} else {

tab_for_boxplot<-tab_rnaseq_select_gene[,colnames(tab_rnaseq_select_gene) %in% colnames(select_column_status)]

list_data[[i]]<- t(tab_for_boxplot)

}


}

}

labTOT<-NULL
valuesTOT<-NULL

for(a in 1:length(list_data[2:5])){

labels<-rep(status_gistic[2:5][a],length(list_data[[a]]))
values<-as.numeric(list_data[[a]])

labTOT<-c(labTOT,labels)
valuesTOT<-c(valuesTOT,values)

}

dfLM<-data.frame(labTOT,valuesTOT)

	blue4="steelblue4"
	blue_alpha4<-add.alpha(blue4,0.4)

	
        blue3="steelblue3"
        blue_alpha3<-add.alpha(blue3,0.4)
	
	snow4="snow4"
	snow4_alpha<-add.alpha(snow4,0.4)
	
	red3="red3"
	red3_alpha<-add.alpha(red3,0.4)
	
	red4="red4"
	red4_alpha<-add.alpha(red4,0.4)
	#i don't want consider the deep loss
        bxresults<-boxplot(list_data[2:5],type="l",lty=1,xaxt="n",lwd=1.5,col=c(blue_alpha4,blue_alpha3,snow4_alpha,red3_alpha,red4_alpha)[2:5],outcol=c(blue_alpha4,blue_alpha3,snow4_alpha,red3_alpha,red4_alpha)[2:5],pch=19,ylab="RSEM",main=gene_for_analysis)
#	mod2<-loess(valuesTOT~labTOT,dfLM)
        axis(1,las=2,labels=names(list_data)[2:5],at=1:length(list_data[2:5]),cex.axis=1)
	#this step is correct 
	##apply a fit using lowess and the median of boxplots

	if(fit==TRUE){	
	lines(lowess(t(bxresults$stats)[,3]),lwd=3,col="dodgerblue4")
	}

	#use shapiro test to test if the distribution are normal
	amplification_vs_gain<-wilcox.test(list_data[[5]],list_data[[4]])$p.value
	amplification_vs_diploid<-wilcox.test(list_data[[5]],list_data[[3]])$p.value
	amplification_vs_shallow<-wilcox.test(list_data[[5]],list_data[[2]])$p.value

	gain_vs_diploid<-wilcox.test(list_data[[4]],list_data[[3]])$p.value
	gain_vs_shallow<-wilcox.test(list_data[[4]],list_data[[2]])$p.value	
	plot(1:10,pch="")

	text(x=3,y=8,label= amplification_vs_gain,cex=0.5)
	text(x=3,y=7,label= amplification_vs_diploid,cex=0.5)
	text(x=3,y=6,label= amplification_vs_shallow,cex=0.5)
	text(x=3,y=5,label= gain_vs_diploid,cex=0.5)
	text(x=3,y=4,label= gain_vs_shallow,cex=0.5)
	
	text(x=6,y=8,label= paste(c("Amplification VS Gain")),cex=0.5)
        text(x=6,y=7,label= paste(c("Amplification VS Diploid")),cex=0.5)
        text(x=6,y=6,label= paste(c("Amplification VS Shallow")),cex=0.5)
        text(x=6,y=5,label= paste(c("Gain VS Diploid")),cex=0.5)
        text(x=6,y=4,label= paste(c("Gain VS Shallow")),cex=0.5)

dev.off()
